mod player;
mod setup;
mod state;
mod unit;
mod world;

use crate::setup::SetupPlugin;
use bevy::prelude::*;

fn main() {
    App::new().add_plugins(SetupPlugin).run();
}
