use crate::player::{Player, PlayerBundle};
use crate::state::GameState;
use crate::unit::UnitBundle;
use bevy::prelude::*;
use bevy::sprite::{MaterialMesh2dBundle, Mesh2dHandle};

pub struct WorldPlugin;

impl Plugin for WorldPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(OnEnter(GameState::InGame), spawn_world);
    }
}

fn spawn_world(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<ColorMaterial>>,
) {
    commands.spawn(PlayerBundle {
        unit: UnitBundle {
            mesh: MaterialMesh2dBundle {
                mesh: Mesh2dHandle(meshes.add(Triangle2d::new(
                    Vec2::Y * 25.0,
                    Vec2::new(-25.0, -25.0),
                    Vec2::new(25.0, -25.0),
                ))),
                material: materials.add(Color::hsl(360.0, 0.95, 0.7)),
                transform: Transform::from_xyz(0.0, 0.0, 0.0),
                ..default()
            },
            ..Default::default()
        },
        player: Player,
    });
}
