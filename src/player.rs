use crate::state::GameState;
use crate::unit::*;
use bevy::prelude::*;

pub struct PlayerPlugin;

#[derive(Component)]
pub struct Player;

#[derive(Bundle)]
pub struct PlayerBundle {
    pub unit: UnitBundle,
    pub player: Player,
}

impl Default for PlayerBundle {
    fn default() -> Self {
        Self {
            unit: UnitBundle {
                ..Default::default()
            },
            player: Player,
        }
    }
}
impl Plugin for PlayerPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(Update, handle_input.run_if(in_state(GameState::InGame)));
    }
}

fn handle_input(
    mut query: Query<(&mut Acceleration, &mut Velocity), With<Player>>,
    keys: Res<ButtonInput<KeyCode>>,
) {
    let (mut acceleration, mut velocity) = query.single_mut();
    acceleration.x = 0.;
    acceleration.y = 0.;

    if keys.pressed(KeyCode::KeyW) {
        acceleration.y = acceleration.rate;
    }

    if keys.pressed(KeyCode::KeyS) {
        acceleration.y = acceleration.rate * -1.;
    }

    if keys.pressed(KeyCode::KeyD) {
        acceleration.x = acceleration.rate;
    }

    if keys.pressed(KeyCode::KeyA) {
        acceleration.x = acceleration.rate * -1.;
    }
}
