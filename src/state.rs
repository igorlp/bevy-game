use bevy::prelude::*;

pub struct StatePlugin;

#[derive(States, Debug, Clone, PartialEq, Eq, Hash)]
pub enum GameState {
    _LoadingScreen,
    _MainMenu,
    InGame,
}

impl Plugin for StatePlugin {
    fn build(&self, app: &mut App) {
        app.insert_state(GameState::InGame);
    }
}
