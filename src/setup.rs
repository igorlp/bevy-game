use crate::player::{Player, PlayerPlugin};
use crate::state::StatePlugin;
use crate::unit::UnitPlugin;
use crate::world::WorldPlugin;
use bevy::core::FrameCount;
use bevy::prelude::*;
use bevy::render::settings::RenderCreation::Automatic;
use bevy::render::settings::{Backends, WgpuSettings};
use bevy::render::RenderPlugin;
use bevy::window::PresentMode;
use bevy_inspector_egui::quick::{FilterQueryInspectorPlugin, WorldInspectorPlugin};
use bevy_screen_diagnostics::{ScreenDiagnosticsPlugin, ScreenFrameDiagnosticsPlugin};

pub struct SetupPlugin;

impl Plugin for SetupPlugin {
    fn build(&self, app: &mut App) {
        app.add_plugins((
            DefaultPlugins
                .set(WindowPlugin {
                    primary_window: Some(Window {
                        title: "Bevy".into(),
                        present_mode: PresentMode::AutoVsync,
                        visible: false,
                        ..default()
                    }),
                    ..default()
                })
                .set(RenderPlugin {
                    render_creation: Automatic(WgpuSettings {
                        backends: Some(Backends::DX12),
                        ..default()
                    }),
                    synchronous_pipeline_compilation: false,
                }),
            ScreenDiagnosticsPlugin::default(),
            ScreenFrameDiagnosticsPlugin,
            WorldInspectorPlugin::default(),
            FilterQueryInspectorPlugin::<With<Player>>::default(),
            StatePlugin,
            WorldPlugin,
            UnitPlugin,
            PlayerPlugin,
        ))
        .add_systems(Startup, setup_window)
        .add_systems(Update, make_window_visible);
    }
}

fn setup_window(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}

fn make_window_visible(mut window: Query<&mut Window>, frames: Res<FrameCount>) {
    if frames.0 == 3 {
        window.single_mut().visible = true;
    }
}
