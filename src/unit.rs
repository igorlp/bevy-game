use bevy::{prelude::*, sprite::MaterialMesh2dBundle};

pub struct UnitPlugin;

#[derive(Component, Reflect)]
pub struct Health {
    pub current: f32,
    pub max: f32,
}

#[derive(Component, Reflect)]
pub struct Velocity {
    pub x: f32,
    pub y: f32,
    pub max: f32,
}

#[derive(Component, Reflect)]
pub struct Acceleration {
    pub x: f32,
    pub y: f32,
    pub rate: f32,
}

#[derive(Bundle)]
pub struct UnitBundle {
    pub health: Health,
    pub velocity: Velocity,
    pub acceleration: Acceleration,
    pub mesh: MaterialMesh2dBundle<ColorMaterial>,
}

impl Default for UnitBundle {
    fn default() -> Self {
        Self {
            health: Health {
                current: 5.,
                max: 5.,
            },
            velocity: Velocity {
                x: 0.,
                y: 0.,
                max: 1000.,
            },
            acceleration: Acceleration {
                x: 0.,
                y: 0.,
                rate: 1000.,
            },
            mesh: Default::default(),
        }
    }
}

impl Plugin for UnitPlugin {
    fn build(&self, app: &mut App) {
        app.add_systems(FixedUpdate, handle_movement)
            .register_type::<Health>()
            .register_type::<Velocity>()
            .register_type::<Acceleration>()
            .register_type::<Direction>();
    }
}

fn handle_movement(
    mut query: Query<(&mut Velocity, &mut Transform, &Acceleration)>,
    time: Res<Time>,
) {
    for (mut velocity, mut transform, acceleration) in query.iter_mut() {
        velocity.x = (velocity.x + acceleration.x)
            .min(velocity.max)
            .max(velocity.max * -1.);
        velocity.y = (velocity.y + acceleration.y)
            .min(velocity.max)
            .max(velocity.max * -1.);

        velocity.x *= 1. / (1. + (time.delta_seconds() * 25.));
        velocity.y *= 1. / (1. + (time.delta_seconds() * 25.));

        transform.translation.x += velocity.x * time.delta_seconds();
        transform.translation.y += velocity.y * time.delta_seconds();
    }
}
